package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if (isValid(start, end)) {
            return generateTable(start, end);
        }
        return null;
    }

    public Boolean isValid(int start, int end) {
        return isStartNotBiggerThanEnd(start, end) && isInRange(start) && isInRange(end);
    }

    public Boolean isInRange(int number) {
        return 1 <= number && number <= 1000;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        return start <= end;
    }

    public String generateTable(int start, int end) {
        StringBuilder table = new StringBuilder();
        for (int i = start; i < end; i++) {
            table.append(generateLine(start, i));
            table.append("\n");
        }
        table.append(generateLine(start, end));
        return table.toString();
    }

    public String generateLine(int start, int row) {
        StringBuilder line = new StringBuilder();
        for (int i = start; i < row; i++) {
            line.append(generateSingleExpression(i, row));
            line.append("  ");
        }
        line.append(generateSingleExpression(row, row));
        return line.toString();
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        int result = multiplicand * multiplier;
        String singleExpression = String.format("%d*%d=%d", multiplicand, multiplier, result);
        return singleExpression;
    }
}
